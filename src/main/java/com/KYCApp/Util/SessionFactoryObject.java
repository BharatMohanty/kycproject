package com.KYCApp.Util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionFactoryObject {
  private static SessionFactory s = null;
  private SessionFactoryObject() {
	  
  }
  public static SessionFactory getConnection() {
	  if(s==null) {
	  Configuration c = new Configuration();
	  c.configure();
	  s = c.buildSessionFactory();
	  return s;
	  }
	 return s;
		  
  }
  
}
