package com.KYCApp.Repository;


import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.KYCApp.Entity.Document;
import com.KYCApp.Util.SessionFactoryObject;





public class KYCRepository {

	public void saveDocument(Document doc) {
	
		SessionFactory sf = SessionFactoryObject.getConnection();
		Session s = sf.openSession();
		Transaction t = s.beginTransaction();
		s.save(doc);
		t.commit();
		
	}
	
	public Document getdocument(Long id) {
		SessionFactory sf = SessionFactoryObject.getConnection();
		Session s = sf.openSession();
		return s.get(Document.class, id);
	}
	
	public boolean DeletById(Long id) {
		Document d = getdocument(id);
		if(d == null)return false;
		else {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sf = SessionFactoryObject.getConnection();
			Session s = sf.openSession();
			Transaction t = s.beginTransaction();
			s.delete(d);
			t.commit();
			return true;
		}
	}
	
	public boolean UpdateContactNumberById(Long id,Long cn) {
		Document d = getdocument(id);
		if(d == null)return false;
		else {
			d.setContactnumber(cn);
			try {
				Configuration cfg = new Configuration();
				cfg.configure();
				SessionFactory sf = SessionFactoryObject.getConnection();
				Session s = sf.openSession();
				Transaction t = s.beginTransaction();
				s.update(d);
				t.commit();
			} catch (HibernateException e) {
				// TODO: handle exception
			}
			return true;
		}
	}
	
	public List<Document> FindAll(){
		SessionFactory sf = SessionFactoryObject.getConnection();
		Session s = sf.openSession();
		String hql = "From Document";
		Query q = s.createQuery(hql);
		return q.list();
	}
	
	
	public List<Document> FindAllByDocument(String dt){
		SessionFactory sf = SessionFactoryObject.getConnection();
		Session s = sf.openSession();
		String hql = "From Document where documenttype = :g";
		Query q = s.createQuery(hql);
		q.setParameter("g", dt);
		return q.list();
	
}
}
	

