package com.KYCApp.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = com.KYCApp.Constants.KycConstants.KYC_INFO)
public class Document implements Serializable{
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
    @GeneratedValue(generator = "m_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "firstnmae")
	private String firstname;
	
	@Column(name = "lastnmae")
	private String lastname;
	
	@Column(name = "contactnumber")
	private Long contactnumber;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "pincode")
	private Long pincode;
	
	@Column(name = "iddocumentverified")
	private Boolean iddocumentverified;
	
	@Column(name = "documenttype")
	private String documenttype;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Long getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(Long contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Long getPincode() {
		return pincode;
	}
	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}
	public Boolean getIddocumentverified() {
		return iddocumentverified;
	}
	public void setIddocumentverified(Boolean iddocumentverified) {
		this.iddocumentverified = iddocumentverified;
	}
	public String getDocumenttype() {
		return documenttype;
	}
	public void setDocumenttype(String documenttype) {
		this.documenttype = documenttype;
	}
	@Override
	public String toString() {
		return "Document [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", contactnumber="
				+ contactnumber + ", city=" + city + ", state=" + state + ", country=" + country + ", pincode="
				+ pincode + ", iddocumentverified=" + iddocumentverified + ", documenttype=" + documenttype + "]";
	}
	
	

}